<?php

namespace App\Console;

use App\Console\Commands\ClassMultipleInheritance;
use App\Console\Commands\ClassReturnsReference;
use App\Console\Commands\ConvertStringsIntoArray;
use App\Console\Commands\ParamInitalizing;
use App\Console\Commands\CirclePolygonRelation;
use App\Console\Commands\CircleSquareRelation;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        CircleSquareRelation::class,
        CirclePolygonRelation::class,
        ParamInitalizing::class,
        ClassReturnsReference::class,
        ClassMultipleInheritance::class,
        ConvertStringsIntoArray::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
