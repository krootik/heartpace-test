<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Arr;

class ConvertStringsIntoArray extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:convert {strings*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Argiments are arrayable-convert strings "var[key1]...=value", separeted by space';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $arrays = $this->getArrayFromStrings();

            foreach ($arrays as $arrayName => &$array) {
                $array = $this->fillArray($array);
            }
            unset($array);

            dump($arrays);
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }

    private function getArrayFromStrings() : array
    {
        $arrayableStrings = $this->argument('strings');
        $arrayKeys = [];
        $arrayValues = [];

        foreach ($arrayableStrings as $element) {
            $elementParts = $this->parseIntoArray($element);

            if (!isset($arrayValues[$elementParts['arr']])) {
                $arrayValues[$elementParts['arr']] = [];
            }

            $keys = explode('][', trim($elementParts['keys'], '[]'));
            $dotKeys = implode('.', $keys);

            foreach ($keys as $keyName) {
                $this->askCharKey($keyName, $arrayKeys);

                $dotKeys = str_replace($keyName, $arrayKeys[$keyName], $dotKeys);
            }

            Arr::set($arrayValues[$elementParts['arr']], $dotKeys, $elementParts['value']);
        }

        return $arrayValues;
    }

    private function fillArray(array $array) : array
    {
        return $this->fillArrayByInputValues(
            $array,
            $this->fillArrayByEmptyStructure($array)
        );
    }

    private function parseIntoArray(string $element) : array
    {
        $elementParts = [];
        if (!preg_match('/^(?<arr>[^\[]+)(?<keys>.*)=(?<value>.*)$/', $element, $elementParts)) {
            throw new \Exception(sprintf('Element %s has missing format. Use "var[key1]...=value"', $element));
        }
        return $elementParts;
    }

    private function askCharKey(string $key, array &$resultKeys) : void
    {
        if (!isset($resultKeys[$key])) {
            do {
                $enteredValue = $this->ask(sprintf('Enter the "%s" value', $key));
            } while((!is_numeric($enteredValue) || $enteredValue < 0) && ($this->error('Only positive integer numbers available') || true));

            $resultKeys[$key] = (int) $enteredValue;
        }
    }

    private function fillArrayByEmptyStructure(array $array) : array
    {
        $arrayDotted = Arr::dot($array);
        $maxElementsByDepth = [];

        collect($arrayDotted)->each(static function($unused, $key) use (&$maxElementsByDepth) {
            $keyLevels = explode('.', $key);
            for($level = 0, $levelMax = count($keyLevels); $level < $levelMax; $level++) {
                $maxElementsByDepth[$level] = max((int) @$maxElementsByDepth[$level], $keyLevels[$level]);
            }
        });
        ksort($maxElementsByDepth);

        return $this->fillArrayRecursive($maxElementsByDepth, head(array_keys($maxElementsByDepth)));
    }

    private function fillArrayByInputValues(array $array, array $resultArray) : array
    {
        $arrayDotted = Arr::dot($array);

        foreach ($arrayDotted as $key => $value) {
            Arr::set($resultArray, $key, $value);
        }

        return $resultArray;
    }

    private function fillArrayRecursive($maxElementsByDepth, $depth)
    {
        if (!isset($maxElementsByDepth[$depth])) {
            return 0;
        }

        return array_fill(0, $maxElementsByDepth[$depth] + 1, $this->fillArrayRecursive($maxElementsByDepth, $depth + 1));
    }
}
