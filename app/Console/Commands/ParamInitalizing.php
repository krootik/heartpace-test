<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ParamInitalizing extends Command
{
    public $v1 = 'default';
    public $v2 = 'value';
    public $v3;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:initializing {--save=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Option "--save=" stores data. Available empty input - then data will be default';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->v3 = implode(' ', [$this->v1, $this->v2]);
        $this->line(sprintf('Initalize value: %s', $this->storeAndGet($this->option('save'))));
    }

    public function storeAndGet($valueToStore) : string
    {
        if (!empty($valueToStore)) {
            $this->v3 = $valueToStore;
        }

        return (string) $this->v3;
    }
}
