<?php

namespace App\Console\Commands;

use App\Helpers\ClassReferenceContainer;
use Illuminate\Console\Command;

class ClassReturnsReference extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:reference';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $referenceContainer = new ClassReferenceContainer();
        $referenceContainer->ref()->var = 'test';
        dump($referenceContainer->ref()->var);
    }
}
