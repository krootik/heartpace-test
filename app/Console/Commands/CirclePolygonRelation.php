<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CirclePolygonRelation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:polygon {r} {xc} {yc} {dots*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'First argument "r" - circle radius; "xc" and "yc" - circle center; other arguments - dots pairs, formatting as "number,number", separate spaces';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $radius = (float) $this->argument('r');
            $circleCenterDots = [(float) $this->argument('xc'), (float) $this->argument('yc')];
            $polygonDotsPairs = $this->transformDotsFromPairs($this->argument('dots'));

            if (empty($radius) || $radius < 0) {
                throw new \Exception('Cicle radius must be greater that 0');
            }
            if (count($polygonDotsPairs) < 3) {
                throw new \Exception('Polygon must contains 3+ pairs of dots');
            }


            if ($this->isPolygonInner($radius, $circleCenterDots, $polygonDotsPairs)) {
                $this->line('Polygon inscribed in the circle');
            } else {
                $this->line('Polygon is not inscribed in the circle');
            }
        } catch (\Exception $e){
            $this->error($e->getMessage());
        }
    }

    protected function transformDotsFromPairs(array $dotsPairs) : array
    {
        $dots = [];
        foreach ($dotsPairs as $pair) {
            $dotsPair = explode(',', $pair);
            if (count($dotsPair) != 2) {
                throw new \Exception(sprintf('Pair "%s" has missing separator format. Use "number,number"', $pair));
            }
            $x = (float) head($dotsPair);
            $y = (float) last($dotsPair);
            if (empty($x) || empty($y)) {
                throw new \Exception(sprintf('Pair "%s" has missing type format. Use "number,number"', $pair));
            }
            $dots[] = [$x, $y];
        }
        return $dots;
    }

    protected function isPolygonInner(float $radius, array $circleCenterDots, array $polygonDotsPairs) : bool
    {
        $isPolygonInner = true;
        $rSqr = pow($radius, 2);
        foreach ($polygonDotsPairs as $pair) {
            $xDiff = head($pair) - head($circleCenterDots);
            $yDiff = last($pair) - last($circleCenterDots);
            if (pow($xDiff, 2) + pow($yDiff, 2) > $rSqr) {
                $isPolygonInner = false;
                break;
            }
        }

        return $isPolygonInner;
    }
}
