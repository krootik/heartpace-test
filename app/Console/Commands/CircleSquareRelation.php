<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CircleSquareRelation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:square {x}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Single argument "x" - is square side length';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $squareSideLength = (float) $this->argument('x');
            if (empty($squareSideLength) || $squareSideLength < 0) {
                throw new \Exception('Square side length must be greater theat 0');
            }

            $maxOuter = $this->getMaxInnerCircleRadius($squareSideLength);
            $minInner = $this->getMinOuterCircleRadius($squareSideLength);
            $availableOuter = mt_rand(2, $maxOuter);
            $availableInner = mt_rand($minInner, $minInner * 10);

            $this->line(sprintf('Max described circle radius: %s', $this->ff($maxOuter)));
            $this->line(sprintf('Min inscribed circle radius: %s', $this->ff($minInner)));
            $this->line(sprintf('Available described circle radius: %s', $this->ff($availableOuter)));
            $this->line(sprintf('Available inscribed circle radius: %s', $this->ff($availableInner)));
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }

    protected function getMaxInnerCircleRadius(float $squareSideLength) : float
    {
        return round($squareSideLength / 2, 2);
    }

    protected function getMinOuterCircleRadius(float $squareSideLength) : float
    {
        return round($squareSideLength / sqrt(2.0), 2);
    }

    /**
     * Format float
     *
     * @param float $num
     *
     * @return string
     */
    protected function ff(float $num) : string
    {
        return number_format($num, 2, '.', '');
    }
}
