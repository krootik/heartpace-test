<?php

namespace App\Console\Commands;

use App\Helpers\MultipleInheritanceClass;
use Illuminate\Console\Command;

class ClassMultipleInheritance extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:inheritance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $multipleInheritanceClass = new MultipleInheritanceClass();

        $this->line($multipleInheritanceClass->method1());
        $this->line($multipleInheritanceClass->method2());
    }
}
