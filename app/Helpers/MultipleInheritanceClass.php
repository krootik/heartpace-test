<?php
/**
 * Created by PhpStorm.
 * User: krootik
 * Date: 02.03.2020
 * Time: 17:06
 */

namespace App\Helpers;

use App\Contracts\Parent1Contract as Parent1;
use App\Contracts\Parent2Contract as Parent2;
use App\Helpers\ContractsDefault\Parent1Contract as Parent1Defaults;
use App\Helpers\ContractsDefault\Parent2Contract as Parent2Defaults;

class MultipleInheritanceClass implements Parent1, Parent2
{
    use Parent1Defaults {method1 as defaultMethod1;}
    use Parent2Defaults;

    public function method1() : string
    {
        return 'extra ' . $this->defaultMethod1();
    }
}