<?php
/**
 * Created by PhpStorm.
 * User: krootik
 * Date: 02.03.2020
 * Time: 16:51
 */

namespace App\Helpers;

class ClassReferenceContainer
{
    protected $ref;

    public function __construct()
    {
        $this->ref = new ClassReference();
    }

    /**
     * По умолчанию объекты и так являются ссылками
     *
     * @return ClassReference
     */
    public function &ref() : ClassReference
    {
        return $this->ref;
    }
}