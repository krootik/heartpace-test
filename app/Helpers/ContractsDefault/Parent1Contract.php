<?php
/**
 * Created by PhpStorm.
 * User: krootik
 * Date: 02.03.2020
 * Time: 17:05
 */

namespace App\Helpers\ContractsDefault;

trait Parent1Contract
{
    public function method1() : string
    {
        return 'method 1';
    }
}