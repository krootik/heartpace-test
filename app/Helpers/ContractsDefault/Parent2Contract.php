<?php
/**
 * Created by PhpStorm.
 * User: krootik
 * Date: 02.03.2020
 * Time: 17:05
 */

namespace App\Helpers\ContractsDefault;

trait Parent2Contract
{
    public function method2() : int
    {
        return 2;
    }
}