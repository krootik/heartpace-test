<?php
/**
 * Created by PhpStorm.
 * User: krootik
 * Date: 02.03.2020
 * Time: 17:03
 */

namespace App\Contracts;

interface Parent1Contract
{
    public function method1() : string;
}