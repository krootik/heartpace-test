<?php
/**
 * Created by PhpStorm.
 * User: krootik
 * Date: 02.03.2020
 * Time: 17:03
 */

namespace App\Contracts;

interface Parent2Contract
{
    public function method2() : int;
}